# QuantonLab's environments

Here are placed all Quanton's lab environment for testing and dev purposes. 


### Usage in Gitlab

```docker
docker login registry.gitlab.com
```

```docker
docker build -t registry.gitlab.com/quanton-lab/environments/[NAME]:[TAG] .
docker push registry.gitlab.com/quanton-lab/environments/[NAME]:[TAG]
```

### Run it

For example to execute phpunit
```docker
docker run -it --name [CONTAINER NAME] --mount type=bind,source=$(pwd),target=/var/www registry.gitlab.com/quanton-lab/environments/[NAME]:[TAG] /bin/bash
```